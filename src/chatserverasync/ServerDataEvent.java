/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chatserverasync;

import java.nio.channels.SocketChannel;

/**
 *
 * @author stan
 */
public class ServerDataEvent {
    	public ChatServerAsync server;
	public SocketChannel socket;
	public byte[] data;
	
	public ServerDataEvent(ChatServerAsync server, SocketChannel socket, byte[] data) {
		this.server = server;
		this.socket = socket;
		this.data = data;
	}
}
